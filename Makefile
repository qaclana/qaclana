VERSION_DATE ?= $(shell date -u +'%Y-%m-%dT%H:%M:%SZ')
CI_COMMIT_SHA ?= $(shell git rev-parse HEAD)
GO_FLAGS ?= GOOS=linux GOARCH=amd64 CGO_ENABLED=0
PACKAGES := $(shell go list . ./cmd/... ./pkg/...)

.DEFAULT_GOAL := build

check:
	@echo Checking...
	@$(foreach file, $(shell go fmt $(PACKAGES) 2>&1), echo "Some files need formatting. Failing." || exit 1)

format:
	@echo Formatting code...
	@go fmt $(PACKAGES)

protoc: 
	@echo Generating stubs...
	@protoc -I . ./pkg/proto/*.proto --go_out=plugins=grpc:.

dep:
	@echo Bringing dependencies...
	@dep ensure

coverage:
	@echo Running tests with coverage report...
	@gocov test $(PACKAGES) -timeout 5s | gocov report

lint:
	@echo Linting...
	@golint $(PACKAGES)
	@gas -quiet -exclude=G104 $(PACKAGES) 2>/dev/null

build: format
	@echo Building...
	@${GO_FLAGS} go build -a -installsuffix cgo -o _output/qaclana -ldflags "-X gitlab.com/qaclana/qaclana/pkg/cmd/version.commitSHA=${CI_COMMIT_SHA} -X gitlab.com/qaclana/qaclana/pkg/cmd/version.date=${VERSION_DATE}"

all: check format protoc dep coverage lint build
ci: all